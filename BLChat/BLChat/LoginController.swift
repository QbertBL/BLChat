//
//  LoginController.swift
//  BLChat
//
//  Created by Владимир Коротыш on 16.04.2018.
//  Copyright © 2018 Владимир Коротыш. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuthUI
import FirebaseGoogleAuthUI

class LoginController: UIViewController {
    let inputContainerView: UIView = {
    let view = UIView()
    view.backgroundColor = UIColor.white
    view.translatesAutoresizingMaskIntoConstraints = false
    view.layer.cornerRadius = 5
    view.layer.masksToBounds = true
    return view
    }()
    lazy var loginRegisterButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = UIColor.rgb(r: 62, g: 95, b: 138)
        button.setTitle("Register", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 5
        button.layer.masksToBounds = true
        button.setTitleColor(UIColor.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(handelRegister), for: .touchUpInside)
        return button
    }()
    @objc func handelRegister(){
        guard let email = emailTextField.text, let password = passTextField.text, let name = nameTextField.text else{
            print("From is not valid")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
           if error != nil{
                print(error)
                return
            }
        guard let uid = user?.uid else {
            return
        }
            let ref = Database.database().reference()
           /* let ref = Database.database().reference(fromURL: "https://blivechat-63ddb.firebaseio.com/")
            let userReference = ref.child("users").child(uid)
            let values = ["name": name, "email": email]
            userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                if err != nil {
                    print(err)
                    return
                }
                self.dismiss(animated: true, completion: nil)
            
            })*/
            ref.child("users").child((user?.uid)!).setValue(["username": name])
        }
        
    }
    

    let nameTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Name"
        tf.translatesAutoresizingMaskIntoConstraints  = false
        return tf
    }()
    let nameSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(r: 0, g: 49, b: 83)
        view.translatesAutoresizingMaskIntoConstraints  = false
        return view
    }()
    let emailTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Email"
        tf.translatesAutoresizingMaskIntoConstraints  = false
        return tf
    }()
    let emailSeparatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor .rgb(r: 0, g: 49, b: 83)
        view.translatesAutoresizingMaskIntoConstraints  = false
        return view
    }()
    let passTextField: UITextField = {
        let tf = UITextField()
        tf.placeholder = "Password"
        tf.translatesAutoresizingMaskIntoConstraints  = false
        tf.isSecureTextEntry = true
        return tf
    }()
    let profileImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.image = UIImage(named: "chat")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    let loginRegisterSegmentControl: UISegmentedControl = {
        let sc = UISegmentedControl(items: ["Login", "Register"])
        sc.translatesAutoresizingMaskIntoConstraints = false
        sc.tintColor = UIColor .white
        sc.selectedSegmentIndex = 1
        sc.addTarget(self, action: #selector(handleLoginRegisterChange), for: .valueChanged)
        return sc
    }()
    @objc func handleLoginRegisterChange() {
        let title = loginRegisterSegmentControl.titleForSegment(at: loginRegisterSegmentControl.selectedSegmentIndex)
        loginRegisterButton.setTitle(title, for: .normal)
        inputsContainerViewHeightAnchor?.constant = loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 100 : 150
        nameTextFieldHeightAnchor?.isActive = false
        nameTextFieldHeightAnchor = nameTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 0 : 1/3)
        nameTextFieldHeightAnchor?.isActive = true
        emailTextFieldHeightAnchor?.isActive = false
        emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        emailTextFieldHeightAnchor?.isActive = true
        passTextFieldHeightAnchor?.isActive = false
        passTextFieldHeightAnchor = passTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 1/2 : 1/3)
        passTextFieldHeightAnchor?.isActive = true
        nameSeparatorViewHeightAnchor?.constant = loginRegisterSegmentControl.selectedSegmentIndex == 0 ? 0 : 1
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = UIColor.rgb(r: 0, g: 49, b: 83)
        view.addSubview(inputContainerView)
        view.addSubview(loginRegisterButton)
        view.addSubview(profileImageView)
        view.addSubview(loginRegisterSegmentControl)
        setupInputsContainerView()
        setupLoginRegisterButton()
        setupProfileImageView()
        setupLoginRegisterSegmentControl()
        
        
        
    }
    func setupProfileImageView() {
        //  constraints profileImageView
            profileImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            profileImageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
            profileImageView.widthAnchor.constraint(equalToConstant: 150).isActive = true
            profileImageView.heightAnchor.constraint(equalToConstant: 150).isActive = true
            inputContainerView.bottomAnchor.constraint(equalTo: inputContainerView.topAnchor, constant: -12).isActive = true
            inputContainerView.widthAnchor.constraint(equalToConstant: 150).isActive = true
            inputContainerView.heightAnchor.constraint(equalToConstant: 150).isActive = true
    }
    func setupLoginRegisterSegmentControl() {
        //constraints LoginRegisterSegmentControl()
        loginRegisterSegmentControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterSegmentControl.bottomAnchor.constraint(equalTo: inputContainerView.topAnchor, constant: -12).isActive = true
        loginRegisterSegmentControl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        loginRegisterSegmentControl.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
    }
        var inputsContainerViewHeightAnchor: NSLayoutConstraint?
        var nameTextFieldHeightAnchor: NSLayoutConstraint?
        var emailTextFieldHeightAnchor: NSLayoutConstraint?
        var passTextFieldHeightAnchor: NSLayoutConstraint?
        var nameSeparatorViewHeightAnchor: NSLayoutConstraint?
    
        func setupInputsContainerView(){
        //  constraints inputContainerView
            inputContainerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            inputContainerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
            inputContainerView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -24).isActive = true
            inputsContainerViewHeightAnchor = inputContainerView.heightAnchor.constraint(equalToConstant: 150)
        
            inputsContainerViewHeightAnchor?.isActive = true
            
            inputContainerView.addSubview(nameTextField)
            inputContainerView.addSubview(nameSeparatorView)
            inputContainerView.addSubview(emailTextField)
            inputContainerView.addSubview(emailSeparatorView)
            inputContainerView.addSubview(passTextField)
        
        //  constraints nameTextField
            nameTextField.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor, constant: 12).isActive = true
            nameTextField.topAnchor.constraint(equalTo: inputContainerView.topAnchor).isActive = true
            nameTextField.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
            nameTextFieldHeightAnchor =  nameTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: 1/3)
            nameTextFieldHeightAnchor?.isActive = true
        //  constraints nameSeparatorView
            nameSeparatorView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
            nameSeparatorView.topAnchor.constraint(equalTo: nameTextField.bottomAnchor).isActive = true
            nameSeparatorView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
            nameSeparatorViewHeightAnchor = nameSeparatorView.heightAnchor.constraint(equalToConstant: 1)
            nameSeparatorViewHeightAnchor?.isActive = true
        //  constraints emailTextField
            emailTextField.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor, constant: 12).isActive = true
            emailTextField.topAnchor.constraint(equalTo: nameSeparatorView.bottomAnchor).isActive = true
            emailTextField.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
            emailTextFieldHeightAnchor = emailTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: 1/3)
            emailTextFieldHeightAnchor?.isActive = true
        //  constraints emailSeparatorView
            emailSeparatorView.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor).isActive = true
            emailSeparatorView.topAnchor.constraint(equalTo: emailTextField.bottomAnchor).isActive = true
            emailSeparatorView.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
            emailSeparatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        //  constraints emailTextField
            passTextField.leftAnchor.constraint(equalTo: inputContainerView.leftAnchor, constant: 12).isActive = true
            passTextField.topAnchor.constraint(equalTo: emailSeparatorView.bottomAnchor).isActive = true
            passTextField.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
            passTextFieldHeightAnchor = passTextField.heightAnchor.constraint(equalTo: inputContainerView.heightAnchor, multiplier: 1/3)
            passTextFieldHeightAnchor?.isActive = true
    }
        func setupLoginRegisterButton(){
        //  constraints LoginRegisterButton
        loginRegisterButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginRegisterButton.topAnchor.constraint(equalTo: inputContainerView.bottomAnchor, constant: 12).isActive = true
        loginRegisterButton.widthAnchor.constraint(equalTo: inputContainerView.widthAnchor).isActive = true
        loginRegisterButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }


}
extension UIColor {
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor{
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha:1)
        
    }
}
