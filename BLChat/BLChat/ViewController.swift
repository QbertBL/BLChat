//
//  ViewController.swift
//  BLChat
//
//  Created by Владимир Коротыш on 16.04.2018.
//  Copyright © 2018 Владимир Коротыш. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuthUI

class ViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
    }
    @objc func handleLogout() {
        let loginController = LoginController()
        present(loginController, animated: true, completion: nil)
        
        
    }


}

